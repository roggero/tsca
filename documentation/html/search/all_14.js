var searchData=
[
  ['var_5fsize',['VAR_SIZE',['../bigint_8h.html#aa2a6f467a65693c173df1ae1780bb2e5',1,'bigint.h']]],
  ['var_5ft',['var_t',['../bigint_8h.html#a7d638512648378d9e7c5fb573cdea8fd',1,'bigint.h']]],
  ['version',['VERSION',['../cipher_8h.html#a1c6d5de492ac61ad29aec7aa9a436bbf',1,'cipher.h']]],
  ['vf',['vf',['../structkey__pair.html#aefd3772554c62c8b50febdafbad90911',1,'key_pair']]],
  ['vf_5finit',['VF_INIT',['../cipher_8h.html#adc5d7aea0e8b2472708e663f174016ff',1,'cipher.h']]],
  ['vi',['vi',['../structkey__pair.html#a6ca43063e5cbdffea02a71d7cfedbf22',1,'key_pair']]],
  ['vi_5finit',['VI_INIT',['../cipher_8h.html#ab91e1bff6f34c20593412a34583fbdf4',1,'cipher.h']]]
];
