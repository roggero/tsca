var searchData=
[
  ['x',['x',['../structpcc__context__s.html#a4557a85e4d8951312e563d8878056a21',1,'pcc_context_s']]],
  ['x2',['x2',['../structpcc__context__s.html#a2a64e2f41fb9985fbcd7cfadf3a814f6',1,'pcc_context_s']]],
  ['xcalloc',['XCALLOC',['../utils_8h.html#addda2d39efa5d32fe2cdb90e6569aca5',1,'XCALLOC():&#160;utils.h'],['../utils_8h.html#a707adcb5c05bf408b0a5ce251c098d65',1,'xcalloc(const char *file, const int line, const char *fnct, const size_t nmemb, const size_t size):&#160;utils.h']]],
  ['xfopen',['XFOPEN',['../utils_8h.html#a1bc00ffa7b68fcc44a658e1f5b1646ff',1,'XFOPEN():&#160;utils.h'],['../utils_8h.html#ac412b45a825ce1c3a77cb46135e46d47',1,'xfopen(const char *file, const int line, const char *fnct, const char *name, const char *mode):&#160;utils.h']]],
  ['xmalloc',['xmalloc',['../utils_8h.html#a9a95dc792728bd04f085587c9d59d24c',1,'xmalloc(const char *file, const int line, const char *fnct, const size_t size):&#160;utils.h'],['../utils_8h.html#a6ba1bf07e47f6a7074d9ecb3347a7ca2',1,'XMALLOC():&#160;utils.h']]],
  ['xor',['xor',['../bigint_8h.html#a99f89255f623cfc380d103a8c20ddb6e',1,'bigint.h']]],
  ['xrealloc',['XREALLOC',['../utils_8h.html#aa1a3466f85f05879949bb9b86bef48ba',1,'XREALLOC():&#160;utils.h'],['../utils_8h.html#adf9dc8165a76431abc624c0fd436bf1f',1,'xrealloc(const char *file, const int line, const char *fnct, void *ptr, const size_t size):&#160;utils.h']]],
  ['xy',['xy',['../structpcc__context__s.html#a1c638c85f566afaad79a7e6fc4a56218',1,'pcc_context_s']]]
];
