%
% Copyright (C) EURECOM, Telecom Paris
%
% Prof. Renaud PACALET          <renaud.pacalet@telecom-paristech.fr>
% Alberto ANSELMO               <Alberto.Anselmo@eurecom.fr>
% Simone Alessandro CHIABERTO	<Simone-Alessandro.Chiaberto@eurecom.fr>
% Fausto CHIATANTE              <Fausto.Chiatante@eurecom.fr>
% Giulio ROGGERO                <Giulio.Roggero@eurecom.fr>
%
% This file must be used under the terms of the CeCILL. This source
% file is licensed as described in the file COPYING, which you should
% have received as part of this distribution. The terms are also
% available at:
% http://www.cecill.info/licences/Licence_CeCILL_V1.1-US.txt 
%

\documentclass[aspectratio=43]{beamer}
\mode<presentation>

\usepackage{header_footer}
\usepackage{pgfpages}
\usepackage{bbm}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{epsfig}
\usepackage{natbib}
\usepackage{apalike}
\usepackage{floatrow}
\usepackage{wasysym}
\usepackage{hyperref}
\usepackage{mathtools}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{caption}
\usepackage[absolute,overlay]{textpos}
%\usepackage[version=4]{mhchem}
\usepackage[long,nodayofweek,level,12hr]{datetime}
\makeatletter
\def\newblock{\beamer@newblock}
\makeatother
\usepackage{color}
\usepackage{lipsum}
\usepackage{graphicx}
%\usepackage{epstopdf}

\definecolor{CASblue}{rgb}{0.611764706, 0.76862745098, 0.95294117647}
\setbeamercolor{frametitle}{fg=CASblue}
\setbeamerfont{frametitle}{series=\bfseries}

\newcommand{\tit}{Timing Side-Channel Attack}
\newcommand{\subt}{Using linear correlation to reveal secrets}
\newcommand{\auth}{A. Anselmo, S.A. Chiaberto, F. Chiatante, G. Roggero}
\newcommand{\inst}{EURECOM}
\newcommand{\supervisor}{Prof. Renaud Pacalet}
\newdate{dat}{21}{06}{2019}
\newcommand{\ddat}{\displaydate{dat}}
\newcommand{\rd}{\quad \ddat \quad \tit}

\setbeamercolor{item}{fg=black}
\setbeamertemplate{itemize items}[circle]
\setbeamertemplate{itemize subitem}[ball]
\setbeamertemplate{itemize subsubitem}[triangle]

\beamertemplatenavigationsymbolsempty

\title{\tit }
\subtitle{\subt}
\author{\auth }
\institute{\inst }
\date{\ddat}

\begin{document}
\input{title.sty}

\presentationtitle{\tit}{\subt}{\auth}{\ddat}{\supervisor}
\begin{frame}
\frametitle{Outline}
\setbeamercolor{section in toc}{fg=black}
\setbeamercolor{subsection in toc}{fg=gray}
\tableofcontents
\end{frame}


%%%%%%%%%%%%%%%%%%%% INTRODUCTION %%%%%%%%%%%%%%%%%%%%%%

\section{Introduction}

\begin{frame}{Leaking informations}
  \begin{alert}{Side-channel attacks}
    \begin{enumerate}
        \pause \item any attack based on information gained from the implementation of a computer system
        \pause \item timing information, power consumption, electromagnetic leaks or even sound can provide an extra source of information
        \pause \item such information are therefore exploitable by an attacker
    \end{enumerate}
  \end{alert}
  \vfill
  \begin{minipage}[t]{0.72\linewidth}
    Therefore, our goal will consist in investigate such leaked information, trying to unveal secrets.
  \end{minipage}
  \hfill%
  \begin{minipage}[c]{0.22\linewidth}
    \vfill
    \includegraphics[width=2cm]{./graphics/leak}
  \end{minipage}
\end{frame}

\begin{frame}
\frametitle{Introduction}
\begin{minipage}[c]{0.22\linewidth}
  \includegraphics[width=2.8cm]{./graphics/corr}
\end{minipage}
\hfill%
\begin{minipage}[c]{0.7\linewidth}
  \begin{itemize}
    \pause \item in several algorithms used for security purposes some optimizations are introduced
    \pause \item these optimizations lead to a linear dependency between time and the data encrypted
    \pause \item knowing information regarding the time-data pair, it is possible to find a correlation
    \pause \item this correlation can be used to unveal part of the secret
  \end{itemize}
\end{minipage}
\end{frame}

\begin{frame}
\frametitle{RSA - our target}
  \begin{center}
    \scalebox{0.75}{
      \begin{minipage}{0.95\linewidth}
        \begin{algorithm}[H]
          \begin{algorithmic}[1]
            \Function{ME}{$D,M,N,NB$}
              \State $C\gets 1*R\bmod N$\Comment{C in Montgomery domain}
              \State $S\gets M*R\bmod N$\Comment{S in Montgomery domain}
              \For{$i\gets 0, NB-1$}
                \If{$D(i) = 1$}
                  \State $C\gets C*S*R^{-1}\bmod N$\Comment{Time dependency}
                \EndIf
                \State $S\gets S*S*R^{-1}\bmod N$
              \EndFor
              \State $C\gets C*R^{-1}\bmod N$\Comment{C back to normal domain}
              \State \textbf{return} $C$
            \EndFunction
          \end{algorithmic}
          \caption{Montgomery Exponentiation, as in \cite{walter1999montgomery}}\label{me}
        \end{algorithm}
      \end{minipage}
    }
  \end{center}
\end{frame}

\subsection{Hypothesis}
\begin{frame}{Hypothesis}
    \begin{block}{Our starting point}
        In order to successfully extract the secret through the correlation, we have to make a list of assumptions:
        \begin{itemize}
            \pause \item timing for a sufficiently large number of cyphertexts is known
            \pause \item plaintexts are known
            \pause \item secret is the same for all encryptions
            \pause \item some operations are timing dependent on the secret and on the plaintext
            \pause \item the HW/SW implementation is known to the attacker
            \pause \item a timing model can be built
        \end{itemize}
    \end{block}
\end{frame}

\subsection{Library development}
\begin{frame}{From the very beginning}
	\begin{block}{BIGINT required}
		In order to operate with large integers, we decided to develop our own library of functions to operate over integers of arbitrary length, in particular with the following elementary instructions:
		\begin{itemize}
			\pause \item addition and subtraction
			\pause \item multiplication
			\pause \item bitwise operation, such as \texttt{AND, OR, XOR, NOT}
			\pause \item logical comparison
		\end{itemize}
	\end{block}
\end{frame}

\begin{frame}{Library testing}
  \begin{block}{Checking the correctness}
    As we wanted to check for formal correctness of the library, we proceed in the following way:
    \begin{enumerate}
      \pause \item design a function written in \textit{C} for our library
      \pause \item emulate its behavior in \textit{Python}, using its infinite precision
      \pause \item launched an intense series of tests to check the formal equality
    \end{enumerate}
  \end{block}

  \pause
  \begin{alert}{An interesting discovery}

    We have found out that the shift bt 32 bits (or multiples) does not produce an effect. This special case has to be handled in our library.
  \end{alert}
\end{frame}

\subsection{Zybo Board}
\begin{frame}{The least complex attack}
	\begin{block}{Bare metal}
		We wanted to exploit the easiest possible attack. Since on a normal device an OS might cause interrupts, thus changing the total time of the enciphering, we decided to:
		\begin{itemize}
			\pause \item compile our code for an ARM architecture
			\pause \item add it to an \textit{Eclipse} project
			\pause \item used the \textsc{Makefile} generated by Xilinx SDK, as explained in \cite{xilinx2015zynq}
			\pause \item copy the executable on the Zybo board
		\end{itemize}
    \begin{center}
      \includegraphics[width=3.5cm]{./graphics/zybo}
    \end{center}
	\end{block}
\end{frame}


%%%%%%%%%%%%%%%%%% ATTACK SECTION %%%%%%%%%%%%%%%%%%%%

\section{Attack}
\subsection{Statistical tool}
\begin{frame}[fragile]
	\frametitle{Finding correlations}
	\begin{block}{PCC: our game changer}
		In order to find the linear contribution of each sample in the overall time, we have used the \textit{Pearson Correlation Coefficient} as an estimator. It has proved to be really effective for our needs, working on the realizations of a random variable.
	\end{block}
	\begin{center}
		\includegraphics[width=6cm]{./graphics/rand_distr}
	\end{center}
\end{frame}

\subsection{Timing Models}

\begin{frame}[allowframebreaks]{Timing Model - ME}
  \scriptsize
  \rule{\textwidth}{0.5pt}
  \captionof{algorithm}{Timing ME Estimate}%
  \vspace*{-0.2cm}
  \rule{\textwidth}{0.5pt}%
  \vspace*{0.2cm}
  \begin{center}
    \begin{algorithmic}[1]
      \Procedure{ME\_ESTIMATE}{$B\_GUESS,M,N,STEP$}
        \For{$i\gets 0, < STEP$}
          \If{$bin(B\_GUESS)(i) = 1$}
            \State $M->C\gets = MM\_estimate(M->C, M->S)$
          \EndIf
          \State $M->S\gets = MM\_estimate(M->S, M->S)$
          \State $T = update\_timing(T)$
        \EndFor
        \State \textbf{return}
      \EndProcedure
    \end{algorithmic}
  \end{center}
\end{frame}

\begin{frame}[allowframebreaks]{Timing Model - MM}
  \scriptsize
  \rule{\textwidth}{0.5pt}
  \captionof{algorithm}{Timing MM Estimate}%
  \vspace*{-0.2cm}
  \rule{\textwidth}{0.5pt}%
  \vspace*{0.2cm}
  \begin{center}
    \begin{algorithmic}[1]
      \Procedure{MM\_ESTIMATE}{$A,B,N,NBIT$}
        \State $RES \gets 0$
        \State $cnt \gets 0$
        \For{$i\gets 0, < NBIT$}
          \State $TMP\gets RES + (A\&B)$
          \If{$LSB(TMP) = 1$}
            \State $Qi\gets 1$
          \Else
            \State $Qi\gets 0$
          \EndIf
          \If{$LSB(A) = 1$}
            \State $RES \gets RES + b $
            \State $cnt++$
          \EndIf
          \If{$Qi = 1$}
            \State $RES \gets RES + N $
            \State $cnt++$
          \EndIf
          \State $RES \: lsr \: 1$
          \State $A \: lsr \: 1$
        \EndFor
        \State \textbf{return}
      \EndProcedure
    \end{algorithmic}
  \end{center}
\end{frame}


\subsection{Algorithm}

\begin{frame}{Algorithm}
    \begin{block}{Development}
        \begin{itemize}
            \pause \item Started in Python to allow better flexibility and library support (infinite precision, \texttt{scipy.stats})
            \pause \item At first, attacking conditional Montgomery Mult., 1 bit at-a-time, using fixed threshold
            \pause \item Move on to attack both MM to improve statistical relevance of 0 guesses
            \pause \item Get rid of fixed threshold by:
            \begin{itemize}
              \item using multi bit analysis
              \item taking max between the accumulated PCCs on a common path
            \end{itemize}
        \end{itemize}
    \end{block}
\end{frame}


\begin{frame}[fragile]
    \frametitle{Algorithm}
    \begin{block}{Final implementation}
        \begin{itemize}
            \pause \item Attack at the same time the two Montgomery moltiplications present in an RSA iteration
            \pause \item Timing estimate: dummy Montgomery moltiplication which evaluates the number of taken branches
            \pause \item Multi bit guessing
            \pause \item Error-detection capabilities
        \end{itemize}
    \end{block}
    \hfill \includegraphics[width=2cm]{./graphics/algorithm}
\end{frame}

\begin{frame}[c]{Demo time}
  \begin{minipage}[c]{0.55\textwidth}
      \begin{center}
        Here's an example with:
        \begin{itemize}
          \item 128 bit keys
          \item 10000 x (plaintext, time)
          \item \textit{C} attack version
        \end{itemize}
      \end{center}
  \end{minipage}
  \hfill
  \begin{minipage}[t]{0.4\textwidth}
    \vfill
      \begin{center}
        \includegraphics[height=3cm]{./graphics/terminal}
      \end{center}
  \end{minipage}
\end{frame}


\begin{frame}[allowframebreaks]{Attack - Pseudocode}
  \scriptsize
  \rule{\textwidth}{0.5pt}
  \captionof{algorithm}{Attack - Final version}%
  \vspace*{-0.2cm}
  \rule{\textwidth}{0.5pt}%
  \vspace*{0.2cm}
  \begin{center}
        %\begin{algorithm}[H]
          \begin{algorithmic}[1]
            \Procedure{ME\_ATTACK}{$D,M,N,NB$}
              \State $G\gets CONSIDERED/GUESSES$\Comment{Useful to group PCCs considered}
              \While{$n\gets 0, < BITS$}
                \For{$i\gets 0, < MSGS$}
                  \For{$j\gets 0, < CONSIDERED$}
                    \State $TM_g(j)(i)\gets ME\_estimate(bin(j))$\Comment{Get the estimate for all possible paths}
                    \State $C(j)(i) = C_g(bin(j))(i)$
                    \State $S(j)(i) = S_g(bin(j))(i)$
                    \State
                  \EndFor
                \EndFor
                \For{$j\gets 0, < CONSIDERED$}
                  \State $PCC\_considered(j)\gets PCC(T\_tot, TM_g(j))$
                \EndFor
                \For{$bit\gets 0, < GUESSES$}
                  \State $G = \{k \mid k \bmod GUESSES = bit\}$
                  \State $$PCC\_guess(bit)\gets \sum_{k \in G} PCC\_considered(k)$$
                \EndFor
                \State $guess = max\_index(PCC\_guess)$\Comment{Assign correct guess}
                \State $D(n:n+GUESSES) = bin(guess)$
                \State $C = C_g(guess)$
                \State $S = S_g(guess)$
                \State $n = n + CONSIDERED$
              \EndWhile
              \State \textbf{return} $D$
            \EndProcedure
          \end{algorithmic}
          %\caption{Attack - Final implementation}\label{att}
        %\end{algorithm}
  \end{center}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Algorithm}
    \begin{block}{Optimizations}
        \begin{itemize}
            \pause \item Completely rewritten in C with $\sim +10x$ speedup over Python
            \pause \item Fully customizable number of bits considered and guessed in one attack iteration
            \pause \item Tweakable filtering of input data with \texttt{\#define} parameters for noisy samples
        \end{itemize}
    \end{block}
    \hfill \includegraphics[width=2cm]{./graphics/tweak}
\end{frame}

\begin{frame}[fragile]
    \frametitle{Algorithm}
    \begin{block}{To have an idea...}
        The C implementations, running on a machine with 2.4Ghz Intel i5:
        \begin{itemize}
            \pause \item cracks 128-bit RSA in 3m40sec
            \pause \item using 10k plaintexts sampled on Zybo board
            \pause \item considering 2 bits and guessing 1 per iteration of the attack
        \end{itemize}
    \end{block}
    \hfill \includegraphics[width=3cm]{./graphics/hacker}
\end{frame}

\subsection{Extremely powerful}
\begin{frame}{Just a simplification}
	\begin{block}{Works on computer also}
		Even if we mainly worked on the \textit{Zybo} board, we can claim that:
		\begin{itemize}
			\pause \item our attack works also when mounted for other devices, including different architectures (Intel x86, ..)
			\pause \item with an OS, more tuples (cipher, timing) are needed
			\pause \item the attack is still feasible
		\end{itemize}
    We have completely tested what is mentioned above. For the OS case, it is relevant to mention that 10 measurements for each encryption are taken, and the smallest is the only considered.\\
    \hfill
    \includegraphics[width=2.8cm]{./graphics/pc}
	\end{block}
\end{frame}

\begin{frame}{Bigger keys}
  \begin{center}
    \includegraphics[width=4cm]{./graphics/key}
  \end{center}
  \begin{block}{RSA on 512/1024/2048/4096}
		The algorithm is capable of handling larger keys on 512, 1024, 2048 and 4096 bits.
    However, the processing time is longer, and a more complex backtrack might be necessary in some cases.
	\end{block}
\end{frame}


%%%%%%%%%%%%%%%%%%%%%%%% COUNTERMEASURES %%%%%%%%%%%%%%%%%%%

\section{Countermeasures}
\begin{frame}{Possible solution}
    \begin{block}{Blinding}
		The proposed countermeasure is the one given in \cite{kocher1996timing}.
		It consists in blinding the message before the encryption using a couple of values $v_f, v_i$ chosen in such a way that:
		\begin{equation*}
			v_i^e \cdot v_f \bmod N = 1
		\end{equation*}
        This contermeasure, in all our tests, has proven to be really effective. Ciphers are completely masked, no correlation can be identified.
    \end{block}
\end{frame}

\begin{frame}[c]{Overhead}
  \begin{block}{Almost no perfomance decrease...}
    Since we are operating in Montgomery domain, we already have an initial operation on the message. It can be combined with the blinding as follows:
    \begin{equation*}
      P_{blind}^{MD} = P \cdot ( R^2 \cdot V_i ) \cdot R^{-1} \bmod N
    \end{equation*}
    The value $R^2 \cdot V_i$ can be computed offline.
    A similar operation can be done at the final conversion to exit the Montgomery domain:
    \begin{equation*}
      C = C_{blind}^{MD} \cdot V_f \cdot R^{-1} \bmod N
    \end{equation*}
  \end{block}
\end{frame}
%--- Next Frame ---%

\section{Future Improvements}
\begin{frame}{Future expectations}
  \begin{block}{What more}
    \begin{itemize}
      \pause \item porting the attack in C++ to keep class structure and speedup w.r. to Python
      \pause \item find an optimal filter and explain the strange behavior of the implemented filter
      \pause \item try to parallelize the estimation for all the messages, as every message is data-independent from each other
    \end{itemize}
  \end{block}
\end{frame}

\begin{frame}[c]{Discussion time}
  \begin{center}
    We have attacked up to now, time to defend.\\
    \Huge{\color{red}{Questions?}}\\
    \vfill
    \hfill
    \includegraphics[height=2cm]{./graphics/qm}
  \end{center}
\end{frame}
%--- Next Frame ---%

\begin{frame}{Our team}
  \noindent
  \begin{minipage}[l][\dimexpr.45\textheight-2\fboxsep-2\fboxrule\relax][t]{\dimexpr .495\textwidth-2\fboxsep-2\fboxrule\relax}
      \begin{center}
        \includegraphics[height=2.7cm]{./graphics/anselmo}\\
        ANSELMO, Alberto
      \end{center}
  \end{minipage}%
  \hfill
  \begin{minipage}[r][\dimexpr 0.45\textheight-2\fboxsep-2\fboxrule\relax][t]{\dimexpr .495\textwidth-2\fboxsep-2\fboxrule\relax}%
      \begin{center}
        \includegraphics[height=2.7cm]{./graphics/chiaberto}\\
        CHIABERTO, Simone
      \end{center}
  \end{minipage}%
  \vfill
  \noindent
  \begin{minipage}[l][\dimexpr 0.45\textheight-2\fboxsep-2\fboxrule\relax][t]{\dimexpr .495\textwidth-2\fboxsep-2\fboxrule\relax}%
      \begin{center}
        \includegraphics[height=2.7cm]{./graphics/chiatante}\\
        CHIATANTE, Fausto
      \end{center}
  \end{minipage}%
  \hfill
  \begin{minipage}[r][\dimexpr 0.45\textheight-2\fboxsep-2\fboxrule\relax][t]{\dimexpr .495\textwidth-2\fboxsep-2\fboxrule\relax}%
    \begin{center}
      \includegraphics[height=2.7cm]{./graphics/roggero}\\
      ROGGERO, Giulio
    \end{center}
  \end{minipage}%
\end{frame}

\begin{frame}[allowframebreaks]
	\frametitle{References}
%\Large{References} \\[0.5cm]
	\footnotesize
	\bibliographystyle{apalike}
	\bibliography{./bibliography/LaTeX}
\end{frame}

\end{document}
